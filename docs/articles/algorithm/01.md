#### JS中的数据结构和标准的对应

| 标准结构      | js中的           |
| ------------- | ---------------- |
| 数组          | Array            |
| 栈            | 无，需要自己实现 |
| 队列          | 无，需要自己实现 |
| 链表          | 无，需要自己实现 |
| 字典          | Object           |
| 散列表/哈希表 | Map              |
| 集合          | Set              |
| 树            | 无，需要自己实现 |
| 图            | 无，需要自己实现 |
| 堆            | 无，需要自己实现 |


![image-20230926114649662](/public/imgs/js/image-20230926114649662.png)



### 一、数组

> 数组（Array）是一种线性表数据结构。它用一组连续的内存空间，来存储一组具有相同类型的数据。

<br>

```js
//初始化数组
var arr = new Array(5).fill(0);
var nums = [1, 3, 2, 5, 4];
```


所谓的线性表就是数据排成一排，想一条线一样的结构。每个线性表上的数据最多只有前和后两个方向。当然除了数组，[链表](https://so.csdn.net/so/search?q=链表&spm=1001.2101.3001.7020)、队列、栈等也是线性表结构

<br>


数组存储在连续的内存空间内，且元素类型相同。这种做法包含丰富的先验信息，系统可以利用这些信息来优化数据结构的操作效率。

- **空间效率高**: 数组为数据分配了连续的内存块，无须额外的结构开销。
- **支持随机访问**: 数组允许在 O(1) 时间内访问任何元素。
- **缓存局部性**: 当访问数组元素时，计算机不仅会加载它，还会缓存其周围的其他数据，从而借助高速缓存来提升后续操作的执行速度。

连续空间存储是一把双刃剑，其存在以下缺点。

- **插入与删除效率低**:当数组中元素较多时，插入与删除操作需要移动大量的元素。
- **长度不可变**: 数组在初始化后长度就固定了，扩容数组需要将所有数据复制到新数组，开销很大。
- **空间浪费**: 如果数组分配的大小超过了实际所需，那么多余的空间就被浪费了。



<br>

**类数组对象**：许多 DOM 对象都是类数组对象——例如 [NodeList](https://developer.mozilla.org/zh-CN/docs/Web/API/NodeList) 和 [HTMLCollection](https://developer.mozilla.org/zh-CN/docs/Web/API/HTMLCollection)。[arguments](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/arguments) 对象也是类数组对象。



对于这种数据，由于不是数组，所以**无法**使用数组上的方法。这样会报错

```js
function f() {
  console.log(arguments.join("+"));
}
f("a", "b"); // Uncaught TypeError: arguments.join is not a function
```

但可以把它当做参数通过**call，apply**传递给数组的方法执行。

```js
function f() {
  console.log(Array.prototype.join.call(arguments, "+"));
}
f("a", "b"); // 'a+b'
```

### 二、栈

[参见栈](/articles/algorithm/02)

### 三、队列

[参见队列](/articles/algorithm/03)

### 四、链表

[参见链表](/articles/algorithm/04)

### 五、字典

js内置Object类型

```js
const obj ={};

obj['a'] = 1;
obj['b'] = 2;
obj['c'] = 3;

```

### 六、哈希表(散列表)

「哈希表 hash table」，又称「散列表」，其通过建立键 `key` 与值 `value` 之间的映射，实现高效的元素查询。具体而言，我们向哈希表输入一个键 `key` ，则可以在O(1) 时间内获取对应的值 `value` 。



##### 哈希冲突与扩容

本质上看，哈希函数的作用是将所有 `key` 构成的输入空间映射到数组所有索引构成的输出空间，而输入空间往往远大于输出空间。因此，理论上一定存在“**多个输入对应相同输出**”的情况。



比如一个班级，有100个人，但是分配给这个班级的学生教师只能坐50个，那么结果必然是，一个座位只能坐2个，甚至2个以上的学生(哈希冲突了)。导致我按桌号点名，站起来了两个以上的学生，这是不对的。

我们得理论情况是，一个座位坐一个学生，按座位点名，每次只站起来一个人。

解决办法就是再增加50个座位(哈希扩容)



因此，**我们可以通过扩容哈希表来减少哈希冲突**。



此方法简单粗暴且有效，但效率太低，因为哈希表扩容需要进行大量的数据搬运与哈希值计算。为了提升效率，我们可以采用以下策略。

1. 改良哈希表数据结构，使得哈希表可以在存在哈希冲突时正常工作。
2. 仅在必要时，即当哈希冲突比较严重时，才执行扩容操作。

哈希表的结构改良方法主要包括“**链式地址**”和“**开放寻址**”。



> 链式地址，好比2个人坐在同一个座位上，我们可以用xx桌号-左边，xx桌号-右边，来区分
>
> 开放寻址，是指一个一个找座位，如果有一个人找的座位已经有人，那么就按照步长(线性探测，或者平方探测)找下一个空座位。



开放寻址以线性探测为例

线性探测采用固定步长的线性搜索来进行探测，其操作方法与普通哈希表有所不同。

- 插入元素：通过哈希函数计算桶索引，若发现桶内已有元素，则从冲突位置向后线性遍历（步长通常为 1 ），直至找到空桶，将元素插入其中
- 查找元素：若发现哈希冲突，则使用相同步长向后线性遍历，直到找到对应元素，返回 `value` 即可；如果遇到空桶，说明目标元素不在哈希表中，返回 None



然而无论是开放寻址还是链地址法，**它们只能保证哈希表可以在发生冲突时正常工作，但无法减少哈希冲突的发生**。



为了减少哈希冲突，我们应该设计更好的hash算法。目前有一些标准的hash算法:

- MD5
- SHA-1 \ SHA-1 \  SHA3

<img src="/public/imgs/algorithm/image-20230926123345501.png" alt="image-20230926123345501" style="zoom:80%;" /> 

更推荐使用 **SHA-2**， js中的内置结构Map，已经内置了hash算法。我们无须关心



优势：
- 无论多少元素，插入、删除、查找的时间复杂度都是 O(1)

缺点：
- 表中的数据没有顺序
- 表中的key不能重复



js内置Map

```js
const map1 = new Map();

map1.set('a', 1);
map1.set('b', 2);
map1.set('c', 3);
```


### 七、集合

一种无序的，不能重复的结构。js内置有Set

```js
let mySet = new Set();

mySet.add(1); // Set [ 1 ]
mySet.add(5); // Set [ 1, 5 ]
mySet.add(5); // Set [ 1, 5 ]
mySet.add("some text"); // Set [ 1, 5, "some text" ]
let o = { a: 1, b: 2 };
mySet.add(o);

mySet.add({ a: 1, b: 2 }); // o 指向的是不同的对象，所以没问题

mySet.has(1); // true
mySet.has(3); // false
mySet.has(5); // true
mySet.has(Math.sqrt(25)); // true
mySet.has("Some Text".toLowerCase()); // true
mySet.has(o); // true

mySet.size; // 5

mySet.delete(5); // true，从 set 中移除 5
mySet.has(5); // false, 5 已经被移除

mySet.size; // 4，刚刚移除一个值

console.log(mySet);
// logs Set(4) [ 1, "some text", {…}, {…} ] in Firefox
// logs Set(4) { 1, "some text", {…}, {…} } in Chrome
```



