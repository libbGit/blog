let sidebar = {
  "/articles/ecology/": [
    {
      text: "CSS基础",
      link: "/articles/ecology",
    },
  ],
  "/articles/html/": [
    {
      text: "HTML基础",
      items: [
        { text: "HTML语义化", link: "/articles/html/01" },
        { text: "客户端表单验证", link: "/articles/html/02" },
        { text: "浏览器工作原理", link: "/articles/html/03" },
        { text: "SVG教程", link: "/articles/html/04" },
      ],
    },
  ],
  "/articles/css/": [
    {
      text: "CSS基础",
      items: [
        { text: "BEM", link: "/articles/css/01" },
        { text: "遇到的问题", link: "/articles/css/02" },
        { text: "css中的权重", link: "/articles/css/03" },
        { text: "设备像素和CSS像素-web", link: "/articles/css/06" },
        { text: "不常见但很有用css", link: "/articles/css/07" },
        { text: "层叠上下文", link: "/articles/css/08" },
        { text: "块格式化上下文", link: "/articles/css/09" },
        { text: "flex布局", link: "/articles/css/10" },
        { text: "css居中布局", link: "/articles/css/11" },
        { text: "Grid布局", link: "/articles/css/12" },
      ],
    },
  ],
  "/articles/scss/": [
    {
      text: "Scss",
      items: [
        { text: "scss基础", link: "/articles/scss/01" },
        { text: "scss中的@规则", link: "/articles/scss/02" },
        { text: "scss中的if、else、for", link: "/articles/scss/03" },
      ],
    },
  ],

  "/articles/js/": [
    {
      text: "js初级",
      items: [
        { text: "基础知识", link: "/articles/js/23" },
        { text: "js数据类型", link: "/articles/js/22" },
        { text: "那些不常见又实用的js知识", link: "/articles/js/02" },
        { text: "事件相关", link: "/articles/js/04" },
        { text: "监听div resize", link: "/articles/js/05" },
        { text: "ECMA规范新特性", link: "/articles/js/06" },
        { text: "深拷贝与浅拷贝", link: "/articles/js/07" },
        { text: "aysnc-await", link: "/articles/js/08" },
        { text: "继承和原型链", link: "/articles/js/11" },
        { text: "闭包", link: "/articles/js/16" },
        { text: "this相关", link: "/articles/js/17" },
        { text: "防抖节流原理", link: "/articles/js/14" },
        { text: "本地存储方案", link: "/articles/js/18" },
      ],
    },
    {
      text: "js中级",
      items: [
        { text: "迭代性", link: "/articles/js/09" },
        { text: "加密算法", link: "/articles/js/12" },
        { text: "异步编程", link: "/articles/js/13" },
        { text: "xss和csrf", link: "/articles/js/15" },
        { text: "正则表达式", link: "/articles/js/20" },
      ],
    },{
      text: "js高级",
      items: [
        { text: "事件循环", link: "/articles/js/01" },
        { text: "微前端沙盒", link: "/articles/js/03" },
        { text: "Blob转换", link: "/articles/js/10" },
        { text: "判断元素是否在可视区域中", link: "/articles/js/19" },
        { text: "PromiseA+", link: "/articles/js/21" },
      ],
    },
  ],

  "/articles/linux/": [
    {
      text: "CI/CD持续集成",
      items: [
        { text: "ESlint", link: "/articles/linux/01" },
        { text: "Linux", link: "/articles/linux/02" },
        { text: "Jenkins脚本", link: "/articles/linux/03" },
        { text: "Jenkins配置", link: "/articles/linux/04" },
        { text: "Shell语法", link: "/articles/linux/05" },
        { text: "devOps配置", link: "/articles/linux/06" },
      ],
    },
  ],

  "/articles/github/": [
    {
      text: "Github",
      items: [
        { text: "git提交规范", link: "/articles/github/01" },
        { text: "npm发布自己的包", link: "/articles/github/02" },
        { text: "git常见问题", link: "/articles/github/issue" },
      ],
    },
  ],

  "/articles/http/": [
    {
      text: "HTTP",
      items: [
        { text: "MIME types", link: "/articles/http/01" },
        { text: "http code", link: "/articles/http/02" },
        { text: "http2", link: "/articles/http/03" },
        { text: "http缓存", link: "/articles/http/04" },
        { text: "条件请求 - HTTP ", link: "/articles/http/05" },
        { text: "内容协商 - HTTP", link: "/articles/http/06" },
        { text: "Cookie - HTTP", link: "/articles/http/07" },
        { text: "请求范围 - HTTP", link: "/articles/http/08" },
        { text: "重定向 - HTTP", link: "/articles/http/09" },
        { text: "权限策略 - HTTP", link: "/articles/http/10" },
        { text: "身份验证 - HTTP", link: "/articles/http/11" },
        { text: "缓存 - HTTP", link: "/articles/http/12" },
        { text: "数据压缩 - HTTP", link: "/articles/http/13" },
        { text: "跨域", link: "/articles/http/14" },
        { text: "CDN机制", link: "/articles/http/15" },
      ],
    },
  ],

  "/articles/nginx/": [
    {
      text: "Nginx",
      items: [
        { text: "负载均衡调度算法", link: "/articles/nginx/01" },
        { text: "nginx配置文件", link: "/articles/nginx/02" },
        { text: "正反向代理", link: "/articles/nginx/03" },
      ],
    },
  ],

  "/articles/vuex/": [
    {
      text: "Vuex",
      items: [{ text: "vuex最完整模板", link: "/articles/vuex/01" }],
    },
  ],

  "/articles/nodejs/": [
    {
      text: "NodeJs",
      items: [
        { text: "npm script执行", link: "/articles/nodejs/01" },
        { text: "CJS模块和ESM模块", link: "/articles/nodejs/02" },
        { text: "npmjs.com中的package.json", link: "/articles/nodejs/03" },
        { text: "package.json官网文档", link: "/articles/nodejs/04" },
        { text: "package.json中的版本", link: "/articles/nodejs/05" },
        { text: "在ts中，package.json的入口配置", link: "/articles/nodejs/06" },
        { text: "node中切换镜像和版本", link: "/articles/nodejs/07" },
      ],
    },
  ],

  "/articles/vscode/": [
    {
      text: "vscode技巧",
      items: [{ text: "正则替换", link: "/articles/vscode/01" }],
    },
  ],

  "/articles/pinia/": [
    {
      text: "Pinia",
      items: [
        { text: "pinia引入", link: "/articles/pinia/01" },
        { text: "store定义 js", link: "/articles/pinia/02" },
        { text: "store定义 ts", link: "/articles/pinia/03" },
        { text: "vue中使用pinia", link: "/articles/pinia/04" },
        { text: "自定义持久化插件", link: "/articles/pinia/05" },
        { text: "在router守卫中使用pinia", link: "/articles/pinia/06" },
      ],
    },
  ],

  "/articles/react/": [
    {
      text: "React",
      items: [
        { text: "vue和react对比", link: "/articles/react/01" },
        { text: "react教程", link: "/articles/react/02" },
        { text: "react中的CSS", link: "/articles/react/03" },
        { text: "react中的路由和store", link: "/articles/react/04" },
      ],
    },
  ],

  "/articles/rollup/": [
    {
      text: "Rollup",
      items: [
        { text: "rollup简介", link: "/articles/rollup/01" },
        { text: "命令行cli", link: "/articles/rollup/02" },
        { text: "rollup.config.js字段", link: "/articles/rollup/03" },
        { text: "常用插件", link: "/articles/rollup/04" },
        { text: "插件开发", link: "/articles/rollup/05" },
      ],
    },
  ],

  "/articles/ts/": [
    {
      text: "TypeScript",
      items: [
        { text: "数据类型", link: "/articles/ts/01" },
        { text: "类型操纵和工具函数", link: "/articles/ts/02" },
        { text: "类型来源", link: "/articles/ts/03" },
        { text: "类型隐式转换关系", link: "/articles/ts/04" },
        { text: "声明方式和 xxx.d.ts", link: "/articles/ts/05" },
        { text: "定义一个tree型接口", link: "/articles/ts/06" },
        { text: "tsconfig.js配置", link: "/articles/ts/07" },
      ],
    },
  ],

  "/articles/vue2/": [
    {
      text: "Vue2",
      items: [
        { text: "vue-loader加载静态资源", link: "/articles/vue2/01" },
        { text: "vue声明周期和父子渲染", link: "/articles/vue2/02" },
        { text: "vue-router原理", link: "/articles/vue2/03" },
        { text: "vue完整单组件结构", link: "/articles/vue2/04" },
        { text: "vue常见问题及解决", link: "/articles/vue2/05" },
        { text: "vue文件中定义多个组件", link: "/articles/vue2/06" },
        { text: "嵌套递归组件使用slot-scope", link: "/articles/vue2/07" },
      ],
    },
  ],

  "/articles/vue3/": [
    {
      text: "Vue3",
      items: [
        { text: "组合式API类型解析", link: "/articles/vue3/01" },
        { text: "Options SFC(TS)", link: "/articles/vue3/02" },
        { text: "Composition SFC(TS)", link: "/articles/vue3/03" },
        { text: "Hook 之间通信", link: "/articles/vue3/04" },
        { text: "v-for下使用slot", link: "/articles/vue3/05" },
        { text: ":deep()的使用", link: "/articles/vue3/06" },
      ],
    },
  ],

  "/articles/problem-solution/": [
    {
      text: "解决方案",
      items: [
        { text: "大文件上传", link: "/articles/problem-solution/01" },
        { text: "虚拟滚动", link: "/articles/problem-solution/02" },
        { text: "Password", link: "/articles/problem-solution/03" },
        { text: "Iframe跨域", link: "/articles/problem-solution/04" },
        { text: "性能优化", link: "/articles/problem-solution/05" },
      ],
    },
  ],

  "/articles/miniprogram/": [
    {
      text: "小程序",
      items: [{ text: "原生微信小程序教程", link: "/articles/miniprogram/01" }],
    },
  ],

  "/articles/monorepo/": [
    {
      text: "Monorepo",
      items: [{ text: "单一存储库", link: "/articles/monorepo/01" }],
    },
  ],

  "/articles/webpack/": [
    {
      text: "Webpack",
      items: [
        { text: "webpack打包时Hash码", link: "/articles/webpack/01" },
        { text: "webpack-chain的用法", link: "/articles/webpack/02" },
      ],
    },
  ],

  "/articles/algorithm/": [
    {
      text: "数据结构",
      items: [
        { text: "数据结构", link: "/articles/algorithm/01" },
        { text: "栈", link: "/articles/algorithm/02" },
        { text: "队列", link: "/articles/algorithm/03" },
        { text: "链表", link: "/articles/algorithm/04" },
        { text: "树", link: "/articles/algorithm/05" },
      ],
    },
    {
      text: "算法",
      items: [
        { text: "算法简介", link: "/articles/algorithm/06" },
        { text: "排序算法", link: "/articles/algorithm/07" },
        { text: "搜索算法", link: "/articles/algorithm/08" },
        { text: "分治算法", link: "/articles/algorithm/09" },
        { text: "回溯算法", link: "/articles/algorithm/10" },
        { text: "贪心算法", link: "/articles/algorithm/11" },
        { text: "动态规划", link: "/articles/algorithm/12" },
      ],
    },
  ],
  "/articles/standard/": [
    {
      text: "规范",
      items: [{ text: "git提交规范", link: "/articles/standard/01" }],
    },
  ],
  "/articles/babel/": [
    {
      text: "babel",
      items: [{ text: "Babel", link: "/articles/babel/01" }],
    },
  ],
};

export default sidebar;
