网格布局（Grid）是最强大的 CSS 布局方案，比flex还要强大。

它将网页划分成一个个网格，可以任意组合不同大小的网格，做出各种各样的布局。
![image.png](/public/imgs/grid/bVbzUqP.png)
如上面的图，正是Grid布局的应用，还有比如 管理系统的主页 dashboard，正是应用了grid布局实现的。

Flex 布局是轴线布局，只能指定"项目"针对轴线的位置，可以看作是一维布局。Grid 布局则是将容器划分成"行"和"列"，产生单元格，然后指定"项目所在"的单元格，可以看作是二维布局。

## 一、概念

容器:采用网格布局的元素

项目：容器的直接子元素(和孙子元素无关)

行: 容器里面的水平区域

列: 容器里面的垂直区域

单元格:行和列的交叉区域
![image.png](/public/imgs/grid/bVbzUt9.png)

网格线:水平网格线划分出行，垂直网格线划分出列。
正常情况下，n行有n + 1根水平网格线，m列有m + 1根垂直网格线，比如三行就有四根水平网格线。
![image.png](/public/imgs/grid/bVbzUuu.png)

Grid属性: 在grid布局中，有两类属性，一类是定义在容器上的，一类是定义在项目中的。

## 二、Grid布局

给对应元素设置 display:grid或者 inline-grid

```vue
<div class="container">
  <span class="item item-1">1</span>
  <span class="item item-2">2</span>
  <span class="item item-3">3</span>
  <span class="item item-4">4</span>
  <span class="item item-5">5</span>
  <span class="item item-6">6</span>
</div>
.container {
  display: grid;
}
```

![image.png](/public/imgs/grid/bVbzUwv.png) 

**默认情况下，子元素都是块级元素，并且默认只有一列**(即grid-template-columns: 只有一个值;)

如果将其设置为display: inline-grid;则可以看到照样是一列，只是宽度没有100%平铺，而是实际宽度。
![image.png](/public/imgs/grid/bVbzUxs.png)


**注意: 设为网格布局以后，容器项目的float、display:inline-block、display:table-cell、vertical-align和column-\*等设置都将失效。**

## 三、容器属性

#### 1、grid-template-columns

定义每一列的列宽。

```css
.container {
  display: grid;
  grid-template-columns: 100px 100px 100px;
}
```

有几个值，表明指定了几列，没定义列宽的列会自动跑到下一行，而多余的列会占用空间。

如下图:
![image.png](/public/imgs/grid/bVbzUyO.png)
![image.png](/public/imgs/grid/bVbzUze.png)

###### 1.1 百分数

表示采用父元素宽度的百分比定义当前列的宽度。

```css
.container {
  display: grid;
  grid-template-columns: 33% 33%;
}
```

![image.png](/public/imgs/grid/bVbzUFh.png) 

###### 1.2 repeat函数

表示重复，第一个参数为几次，后面参数表示每次的值是多少

```css
.container {
  display: grid;
  grid-template-columns: repeat(3, 33.33%);
}
```

repeat(3, 33.33%);等于 33.33% 33.33% 33.33%

![image.png](/public/imgs/grid/bVbzUFU.png) 

###### 1.3 auto-fill 关键字

比如，每列是固定的宽度，可是一行究竟放几列呢？不知道，只知道随着屏幕宽度大小，自动补充剩余空间。
![image.png](/public/imgs/grid/bVbzUGX.png)
![image.png](/public/imgs/grid/bVbzUG3.png)
![image.png](/public/imgs/grid/bVbzUG6.png)

###### 1.4 fr 关键字

fr表示比例关系,定义网格轨道大小的弹性系数,类似于 flex,

```css
.container {
  display: grid;
  grid-template-columns: 1fr 2fr; //表示定义两列，这两列为1比2
}
```

![image.png](/public/imgs/grid/bVbzUIL.png)
上面grid-template-columns: 1fr 2fr;等同于 flex:1, flex:2

甚至可以这么写
grid-template-columns: 150px 1fr 2fr;

###### 1.5 minmax()

minmax()函数产生一个长度范围，表示长度就在这个范围之中。它接受两个参数，分别为最小值和最大值。

```css
.container {
  display: grid;
  grid-template-columns: 1fr 1fr minmax(200px, 1fr);
}
```



###### 1.6 auto 关键字

表示由浏览器自己决定长度。

```css
.container {
  display: grid;
  grid-template-columns: 50px auto auto auto;
}
```

![image.png](/public/imgs/grid/bVbzUNI.png) 

上面grid-template-columns: 50px auto auto auto;等同于
grid-template-columns: 50px 1fr 1fr 1fr;

#### 2、grid-template-rows

定义每一行的行高，默认为子元素的行高

```css
.container {
  display: grid;
  grid-template-rows: 100px 100px 100px;
}
```

有几个值，表明指定了几行，没定义的行会采用默认的行高(不会自动跑到下一列)，而多余的行，会占用空间

如图，只定义了前三行，后面三行均采用默认值。
![image.png](/public/imgs/grid/bVbzUz8.png)

###### 1、grid-template-columns和grid-template-rows组合

默认先按grid-template-columns计算列数，然后自动计算行数。

当grid-template-columns和grid-template-rows组合是会出现，不匹配的情况。

比如一共6个项目，我定义了3列(
grid-template-columns)，那么也就是自动2行，但是此时又指定了3行(grid-template-rows)，那么此时就是3\*3的网格。

而又有 一共6个项目，我定义了3列，那么自动就是2行，此时又指定1行，那么此时不会是3\*1，还是3\*2.

其中grid-template-columns和grid-template-rows指定的网格，我们可以称为标准网格，
而当实际的项目比定义的标准网格要多时。我们称超出的部分，为扩展网格。

标准网格和扩展网格共同组成了Grid布局的网格。

后面我们会通过另外的属性对于扩展网格定义宽高。

#### 3、grid-auto-flow

划分网格以后，容器的子元素会按照顺序，自动放置在每一个网格。默认的放置顺序是"先行后列"，即先填满第一行，再开始放入第二行，即下图数字的顺序。

grid-auto-flow默认值为row，即"先行后列"。如果为column，则先列后行。
![image.png](/public/imgs/grid/bVbzWmK.png)

```css
.container {
  display: grid;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px 100px;
  grid-auto-flow: row;
}
```

![image.png](/public/imgs/grid/bVbzWmP.png) 

```css
.container {
  display: grid;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px 100px;
  grid-auto-flow: column;
}
```

grid-auto-flow不管是row还是column，都是后面的列排不下了，那么会换行，此时，前一行后面就会有空白。

而如果设置成row dense和column dense，则前一行的空白会按顺序找出第一个能刚好放下的元素补齐。

```css
.container {
  display: grid;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px 100px;
  grid-auto-flow: row;
}
.item-1{
  grid-column-start: 1;
  grid-column-end: 3;
}
.item-2{
  grid-column-start: 1;
  grid-column-end: 3;
}
```

此时会看到右上方有个空白，因为item-3是紧跟在item-2后面的。
![image.png](/public/imgs/grid/bVbzWpV.png) 

```css
.container {
  display: grid;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px 100px;
  grid-auto-flow: row dense;
}
.item-1{
  grid-column-start: 1;
  grid-column-end: 3;
}
.item-2{
  grid-column-start: 1;
  grid-column-end: 3;
}
```

而如果改成row dense，则会发现item-1和item-2无法拍到item-1后，换行了，接着按顺序找到了item-3补齐在item-1后面。
![image.png](/public/imgs/grid/bVbzWql.png) 

所以添加dense后，会“尽量填满空格”

#### 4、row-gap

设置行与行的间隔（行间距）

```css
.container {
  display: grid;
  grid-template-columns: 100px 100px 100px;
  grid-template-rows: 100px 100px 100px;
  row-gap: 20px;
}
```

![image.png](/public/imgs/grid/bVbzUOT.png) 

#### 5、column-gap

设置列与列的间隔（列间距）

```css
.container {
  display: grid;
  grid-template-columns: 100px 100px 100px;
  grid-template-rows: 100px 100px 100px;
  row-gap: 20px;
  column-gap: 20px;
}
```

![image.png](/public/imgs/grid/bVbzUPA.png) 

row-gap和column-gap可以简写为
gap:\<row-gap\> \<column-gap\>

如果gap省略了第二个值，浏览器认为第二个值等于第一个值。

```css
.container {
  display: grid;
  grid-template-columns: 100px 100px 100px;
  grid-template-rows: 100px 100px 100px;
  gap: 20px 20px;
}
```

#### 6、grid-template-areas

网格布局允许指定"区域"（area）编号，以便在项目中使用。
（对现有布局不会产生影响）

![image.png](/public/imgs/grid/bVbzWln.png) 

也可以进行合并，即起同样的名字
![image.png](/public/imgs/grid/bVbzWlX.png)

```css
.container {
  display: grid;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px 100px;
  grid-template-areas:
        "header header header"
        "main main sidebar"
        "footer footer footer";
}
```

#### 7、justify-items

设置容器下所有单元格内容的水平位置（左中右），网格线不动，基于网格线的位置。它针对的是所有的单元格。
justify-items: start | end | center | stretch(默认);

```css
.container {
  display: grid;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px 100px;
  justify-items: start;
}
```

由于不是stretch后，项目的宽度都是自身的宽
![image.png](/public/imgs/grid/bVbzWr9.png) 

#### 8、align-items

设置容器下所有单元格内容的垂直位置（上中下），网格线不动，基于网格线的位置。它针对的是所有的单元格。
align-items: start | end | center | stretch(默认);

```css
.container {
  display: grid;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px 100px;
  align-items: start;
}
```

![image.png](/public/imgs/grid/bVbzWs3.png) 

上面的justify-items和align-items可以合并成一个属性:place-items

place-items: \<align-items\> \<justify-items\>;

```css
.container {
  display: grid;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px 100px;
  place-items: start start;
}
```

![image.png](/public/imgs/grid/bVbzWtH.png) 

#### 9、justify-content

整个内容区域在容器里面的水平位置（左中右），网格线会动，基于的是grid的边缘。
如果存在至少一个弹性元素且填满剩余空间，那么对齐方式不会生效，就像没有多余空间的情况。

```applescript
justify-content: start | end | center | stretch | space-around | space-between | space-evenly;
```

space-around和space-evenly区别在与，space-around指项目和项目之间的距离相等，但是离边缘是项目之间的一半，而space-evenly即使再边缘也等于项目之间的距离。

```css
.container {
  display: grid;
  width: 600px;
  height: 400px;
  background-color: antiquewhite;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px 100px;
  justify-content: space-around;
}
```

![image.png](/public/imgs/grid/bVbzWwK.png) 

```css
.container {
  display: grid;
  width: 600px;
  height: 400px;
  background-color: antiquewhite;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px 100px;
  justify-content: space-evenly;
}
```

![image.png](/public/imgs/grid/bVbzWw8.png) 

#### 10、align-content

整个内容区域在容器里面的垂直位置（上中下），网格线会动，基于的是grid的边缘。类似于justify-content

align-content: start | end | center | stretch | space-around | space-between | space-evenly;

```css
.container {
  display: grid;
  width: 600px;
  height: 400px;
  background-color: antiquewhite;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px 100px;
  align-content: space-around;
}
```

![image.png](/public/imgs/grid/bVbzWx9.png) 

```css
.container {
  display: grid;
  width: 600px;
  height: 400px;
  background-color: antiquewhite;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px 100px;
  align-content: space-evenly;
}
```

![image.png](/public/imgs/grid/bVbzWyd.png) 

上面align-content和justify-content可以合并为 place-content。

place-content: \<align-content\> \<justify-content\>

```css
.container {
  display: grid;
  width: 600px;
  height: 400px;
  background-color: antiquewhite;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px 100px;
  place-content: space-evenly space-around;
}
```

![image.png](/public/imgs/grid/bVbzWyM.png) 

#### 11、grid-auto-rows

用来设置，通过 grid-template-columns 和 grid-template-rows定义的标准网格大小，比实际的 项目数小时，多余的项目，即扩展网格的列和行的大小。
默认情况下多余的列和行，会使用项目中元素的实际内容的大小。

```css
.container {
  display: grid;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px;
}
```

定义了一个3列，1行的网格，一共可以放置3个项目，而下面是6个项目，也就意味后，item-4到item-6是在网格之外的。那么就使用自身元素的实际内容大小

```vue
<div class="container">
  <span class="item item-1">1</span>
  <span class="item item-2">2</span>
  <span class="item item-3">3</span>
  <span class="item item-4">4</span>
  <span class="item item-5">5</span>
  <span class="item item-6">6</span>
</div>
```

![image.png](/public/imgs/grid/bVbzWAM.png) 

但是我们可以通过grid-auto-rows来显示的指定，多余的部分的行高。它只影响多余的项目，对本来的网格项目不会造成影响。

```css
.container {
  display: grid;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px;
  grid-auto-rows: 50px;
}
```

![image.png](/public/imgs/grid/bVbzWAS.png) 

#### 12、grid-auto-columns

用来设置，通过 grid-template-columns 和 grid-template-rows定义的网格大小，比实际的 项目数小时，多余的项目的列和行的大小。
默认情况下多余的列和行，会使用项目中元素的实际内容的大小。

但是和grid-auto-rows不同的是，grid-auto-rows的多余行，会采用自身元素实际的行高，而grid-auto-columns的多余列，会填满剩余的空间(即使是span也是，奇怪)

```css
.container {
  display: grid;
  grid-auto-flow: column;
  grid-template-columns: 100px;
  grid-template-rows: 100px 100px 100px;
}
```

定义了一个1列，3行的，一供可以放置3个项目。那么第二列就是多余的列。
![image.png](/public/imgs/grid/bVbzWDf.png) 

使用grid-auto-columns后。

```css
.container {
  display: grid;
  grid-auto-flow: column;
  grid-template-columns: 100px;
  grid-template-rows: 100px 100px 100px;
  grid-auto-columns: 20px;
}
```

![image.png](/public/imgs/grid/bVbzWEM.png) 

## 四、项目属性

前面的12个属性都是定义在容器上的，而下面的这些属性则是定义在容器里面的项目上的。

#### 1、grid-column-*

grid-column-start 定义项目垂直方向的 的起始网格
grid-column-end 定义项目垂直方向的终止网格（auto|span n|column-line）

```scss
.container {
  display: grid;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px 100px;
}

.item-1{
   grid-column-start: 2;  //项目item-1起始起始方向从第二列开始
   grid-column-end: 4;  //项目item-1到第四列截止，共占2列，
}
或者
.item-1{
   grid-column-start: 2;  //项目item-1起始起始方向从第二列开始
   grid-column-end: span 2;  //跨越2列，截止列为第4列
}
```

![image.png](/public/imgs/grid/bVc6J5t.png) 

#### 2、grid-row-*

grid-row-start 定义项目水平方向的 的起始网格（整数。比如grid-template-rows定义了3个值，那么grid-column-start可以取1到4）
grid-row-end 定义项目水平方向的 的终止网格（整数。比如grid-template-rows定义了3个值，那么grid-column-start可以取1到4）

```css
.container {
  display: grid;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px 100px;
}

.item-1{
   grid-row-start: 1;
   grid-row-end: 3;
}
```

item-1垂直方向从1开始，到3结束
![image.png](/public/imgs/grid/bVc6Kc2.png) 

上面的*-start和*-end可以改成网格线的名称。

```css
.container {
  display: grid;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px;
  grid-template-areas:
        "header header setting"
        "main main sidebar"
}
```

上面第一行定义了两列 header setting，所以他们的网格线分别为 header-start，header-end和setting-start，setting-end。其他也一样。

```css
.item-1{
  grid-column-start: header-start;
  grid-column-end: header-end;
  grid-row-start: main-start;
  grid-row-end: footer-end;
}
```

![image.png](/public/imgs/grid/bVbzWPW.png) 

#### 3、grid-area

指定项目放在哪一个区域

```css
.container {
  display: grid;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px;
  grid-template-areas:
        "header header setting"
        "main main sidebar"
}
.item-1{
  grid-area: header;
}
```

![image.png](/public/imgs/grid/bVbzWTg.png) 

其实我们发现
grid-area: header;
和下面的效果一模一样

```css
 grid-row-start: header;
 grid-column-start: header;
 grid-row-end: header;
 grid-column-end: header;
```

#### 4、justify-self

设置单元格内容的水平位置（左中右），跟justify-items属性的用法完全一致，但只作用于单个项目。
justify-self: start | end | center | stretch;

```css
.container {
  display: grid;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px;
}
.item-1{
  justify-self: center;
}
```

![image.png](/public/imgs/grid/bVbzWTP.png) 

#### 5、align-self

设置单元格内容的垂直位置（上中下），跟align-items属性的用法完全一致，但只作用于单个项目。
align-self: start | end | center | stretch;

```css
.container {
  display: grid;
  grid-template-columns: 100px 168px 100px;
  grid-template-rows: 137px 100px;
}
.item-1{
   align-self: center;
}
```

![image.png](/public/imgs/grid/bVbzWUh.png) 

还可用 place-self属性
place-self: \<align-self\> \<justify-self\>;

**注意:**
flex的子元素宽度是实际宽度，而高度却是容器高度。

```css
justify-content  //默认flex-start，

align-content   //默认stretch,拉伸，占满单元格的整个宽度
align-items   //默认stretch,拉伸，占满单元格的整个宽度
```

grid布局是比flex更加强大和灵活的布局，他们的子元素宽度和高度均是等比填充容器，

```css
justify-content //默认stretch,拉伸，占满单元格的整个宽度
justify-items //默认stretch,拉伸，占满单元格的整个宽度

align-content //默认stretch,拉伸，占满单元格的整个宽度
align-items //默认stretch,拉伸，占满单元格的整个宽度
```

grid居中一个元素，既可以使用

```css
justify-content:center 
align-content:center
```

也可以

```css
justify-items:center 
align-items:center
```

**常用属性总结如下:**
容器属性
grid-auto-flow
grid-template-columns / grid-template-rows
row-gap / column-gap
justify-content / align-content
justify-items / align-items

项目属性
grid-column-start / grid-column-end
grid-row-start / grid-row-end
justify-self / align-self