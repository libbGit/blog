import { defineConfig } from "vitepress";
import { SearchPlugin } from "vitepress-plugin-search";
import topNav from "./nav.mjs";
import sidebar from "./sidebar.mjs";

const options = {
  previewLength: 62,
  buttonLabel: "搜索",
  placeholder: "在文档中搜索",
  allow: [],
  ignore: [],
};
// https://vitepress.dev/reference/site-config
export default defineConfig({
  head: [
    ['link', { rel: 'icon', href: `/blog/logo.svg` }]
  ],

  vite: { plugins: [SearchPlugin(options)] },

  base:"/blog/",
  outDir: '../dist',
  lang: "zh-CN",
  srcDir: ".",
  title: "哔叽",
  description: "哔叽",

  markdown: {
    theme: "material-theme-palenight",
    lineNumbers: true,
  },

  themeConfig: {
    localSearch: true,
    outline: "deep",
    outlineTitle: "目录",
    lastUpdated: true,
    lastUpdatedText: "Updated Date",

    // https://vitepress.dev/reference/default-theme-config
    nav: topNav,

    sidebar: sidebar,

    socialLinks: [
      { icon: "github", link: "https://gitee.com/minipear/blog" },
      {
        icon: {
          svg: '<svg role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>Dribbble</title><path d="M12...6.38z"/></svg>',
        },
        link: "...",
      },
    ],

    footer: {
      message: "Released under the MIT License.",
      copyright: "Copyright © 2020-2025 libingbing",
    },
  },
});
