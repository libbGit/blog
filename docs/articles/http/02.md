http code 大全



[信息响应](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Status#信息响应) 

1. 100 Continue
2. 101 Switching Protocols  
3. 103 Early Hints

[成功响应](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Status#成功响应) 

1. 200 OK
2. 201 Created
3. 202 Accepted
4. 203 Non-Authoritative Information
5. 204 No Content
6. 205 Reset Content
7. 206 Partial Content

[重定向消息](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Status#重定向消息)

1. 300 Multiple Choices
2. 301 Moved Permanently   永久性重定向(GET方法不会发生变更。其他方法有可能会变更为GET 方法), 场景为网站重构。
3. 302 Found   临时性重定向(GET方法不会发生变更。其他方法有可能会变更为GET方法)，场景为站点维护或由于不可预见的原因该页面暂不可用。
4. 303 See Other   临时性重定向       通常作为 [PUT](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Methods/PUT) 或 [POST](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Methods/POST) 操作的返回结果，它表示重定向链接指向的不是新上传的资源，而是另外一个页面，比如消息确认页面或上传进度页面
5. 304 Not Modified      它告诉客户端响应还没有被修改，因此客户端可以继续使用相同的缓存版本的响应。通常用于协商缓存时返回
6. 307 Temporary Redirect     302的改进（方法和消息主体都不发生变化），场景由于不可预见的原因该页面暂不可用。
7. 308 Permanent Redirect     301的改进（方法和消息主体都不发生变化），场景为使用用于非 GET 链接/操作重组网站

[客户端错误响应](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Status#客户端错误响应)

1. 400 Bad Request  由于被认为是客户端错误（错误的请求语法、无效的请求消息）
2. 401 Unauthorized   未授权，需要用户凭证，一般session过期
3. 402 Payment Required     直到客户端付费之后请求才会被处理
4. 403 Forbidden   拒绝授权访问
5. 404 Not Found   地址url错误，未找到
6. 405 Method Not Allowed  请求方式不对，比如应该是get，结果用了post
7. 406 Not Acceptable
8. 407 Proxy Authentication Required
9. 408 Request Timeout    服务器想要将没有在使用的连接关闭
10. 409 Conflict
11. 410 Gone
12. 411 Length Required
13. 412 Precondition Failed
14. 413 Payload Too Large
15. 414 URI Too Long
16. 415 Unsupported Media Type
17. 416 Range Not Satisfiable
18. 417 Expectation Failed
19. 418 I'm a teapot
20. 422 Unprocessable Entity
21. 425 Too Early
22. 426 Upgrade Required
23. 428 Precondition Required
24. 429 Too Many Requests
25. 431 Request Header Fields Too Large
26. 451 Unavailable For Legal Reasons

[服务端错误响应](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Status#服务端错误响应) 

1. 500 Internal Server Error 服务器出现问题
2. 501 Not Implemented   服务器不支持请求方法，因此无法处理
3. 502 Bad Gateway   作为网关或代理的服务器，从上游服务器中接收到的响应是无效的。
4. 503 Service Unavailable    服务器因维护或重载而停机
5. 504 Gateway Timeout   无法及时获得响应时，请求超时
6. 505 HTTP Version Not Supported    服务器不支持请求中使用的 HTTP 版本。
7. 506 Variant Also Negotiates
8. 507 Insufficient Storage
9. 508 Loop Detected
10. 510 Not Extended    服务器需要对请求进行进一步扩展才能完成请求
11. 511 Network Authentication Required  服务器需要对请求进行进一步扩展才能完成请求。