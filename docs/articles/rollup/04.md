

4、常用插件

https://github.com/rollup/awesome

```js
    "@rollup/plugin-alias": "^3.1.9",        打包时定义别名
    "@rollup/plugin-babel": "^5.3.1",    用于将现在es6转为能在旧浏览器上支持的代码，额外需要 @babel/core @babel/preset-env
    "@rollup/plugin-commonjs": "^22.0.1", 用于将CommonJS模块转换为ES6，以便将它们包含在rollup包中
    "@rollup/plugin-image": "^2.1.1", 用来导入JPG、PNG、GIF、SVG和WebP文件
    "@rollup/plugin-node-resolve": "^13.3.0",  允许rollup加载node_modules中的第三方模块
    "@rollup/plugin-replace": "^4.0.0",    在打包时替换文件中的目标字符串。
     @rollup/plugin-json      将.json文件转换为ES6模块。
    
    "rollup-plugin-copy": "^3.4.0",     复制文件和文件夹
    "rollup-plugin-esbuild": "^4.10.1",   使用esbuild编译js+ts和最小化
    "rollup-plugin-postcss": "^4.0.2",     Rollup和POSTSS之间的无缝集成。
    "rollup-plugin-terser": "^7.0.2",   使用 terser编译和最小化
    "rollup-plugin-vue": "^6.0.0",      将Vue 3 单文件进行打包。
```

