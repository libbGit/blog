---

# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: 哔叽
  tagline: 这是一个类似于博客的，面向工作总结的一个网站
  image:
    src: /logo.png
    alt: minipear
  actions:
    - theme: alt
      text: 查看文章
      link: /articles/ecology.md
    # - theme: alt
    #   text: API Examples
    #   link: /api-examples

features:
  - title: HTML
    icon:
      src: /imgs/html.png
    details: 超文本标记语言，前端三剑客之一，用来定义网页结构
    link: /articles/html/01.html
  - title: CSS
    icon:
      src: /imgs/css.png
    details: 层叠样式表，前端三剑客之一，用来美化网页  
    link: /articles/css/01.html
  - title: JavaScript
    icon:
      src: /imgs/js.png
    details: 前端脚本语言，前端三剑客之一，用来给网页添加事件，行为，逻辑 
    link: /articles/js/01.html
  - title: TypeScript
    icon:
      src: /imgs/typescript.png
    details: 为javascript添加静态类型定义，是JavaScript类型的超集,它可以编译成纯JavaScript 
    link: /articles/ts/01.html
  - title: HTTP
    icon:
      src: /imgs/http.png
    details: 超文本传输协议，简单的请求-响应协议。它指定了客户端可能发送给服务器什么样的消息以及得到什么样的响应
    link: /articles/http/01.html
  - title: VUE
    icon:
      src: /imgs/vue.png
    details: 渐进式JavaScript 框架，易学易用，性能出色，适用场景丰富的 Web 前端框架。
    link: /articles/vue3/01.html
  - title: React
    icon:
      src: /imgs/react.png
    details: 用于构建 Web 和原生交互界面的库
    link: /articles/react/01.html
  - title: Pinia
    icon:
      src: /imgs/pinia.png
    details: Pinia 是 Vue 的存储库，它允许您跨组件/页面共享状态
    link: /articles/pinia/01.html
  - title: Webpack
    icon:
      src: /imgs/webpack.png
    details: 一个用于现代 JavaScript 应用程序的 静态模块打包工具
    link: /articles/webpack/01.html
  - title: Rollup
    icon:
      src: /imgs/rollup.png
    details: JavaScript模块打包器，将小段代码编译成更大、更复杂的代码
    link: /articles/rollup/01.html 
  - title: Nodejs
    icon:
      src: /imgs/nodejs.png
    details: Node.js 是一个免费的、开源的、跨平台的 JavaScript 运行时环境，允许开发人员在浏览器之外编写命令行工具和服务器端脚本.
    link: /articles/nodejs/01.html 
  - title: Linux
    icon:
      src: /imgs/linux.png
    details: 未知
    link: /articles/linux/01.html 
  
  # - title: CSS
  #   details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  #   link: /articles/html/01.html
  # - title: JAVASCRIPT
  #   details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  #   link: /articles/html/01.html
  # - title: VUE
  #   details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  #   link: /articles/html/01.html
  # - title: HTTP
  #   details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  #   link: /articles/html/01.html
---
