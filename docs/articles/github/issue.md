### 一、git 提交时不识别文件大小写

必须要在具有.git 的项目才能生效。

git 在提交代码时，默认会忽略文件名称大小写，导致本地代码与远程代码不一致，此时可利用终端指令来检查下

```sh
git config --get core.ignorecase   # true false
```

若为 true 值则代表当前是忽略的，此时需要将当前项目提交设置为 false

```sh
git config core.ignorecase false
```

注意每个项目都是独立配置的。此设置只对当前 git 项目生效


### 二、检查项目下哪些npm包有更新
```sh
npm i -g  npm-check-updates
```
然后在项目路径下执行
```sh
ncu
```

### 三、提交名称不对

<img src="/public/imgs/git/image-20231113180843053.png" alt="image-20231113180843053.png"/> 

查看当前git提交用户名

`git config --global user.name`

如图所示我们提交名称是 LIBB/libingbing，显示有问题。

我们可以修改下
```sh
git config --global user.name '李兵兵'
git config --global user.email '824610286@qq.com'
```