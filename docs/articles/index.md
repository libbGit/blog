---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: 小李子的每一天
  tagline: 这是一个类似于博客的，面向工作总结的一个网站
  actions:
    - theme: brand
      text: 点击查看更多
      link: /markdown-examples
    - theme: alt
      text: API Examples
      link: /api-examples

features:
  - title: HTML
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
    link: /api-examples
  - title: CSS
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: JAVASCRIPT
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: VUE
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: HTTP
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
---

