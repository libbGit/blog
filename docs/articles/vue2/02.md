父子加载渲染过程

```
父:    beforeCreate ->  created ->  beforeMount ->                                                    mounted
子:                                                beforeCreate -> created -> beforeMount -> mounted ->
```

父子组件更新过程

```
父: beforeUpdate ->                          updated
子:                beforeUpdate -> updated ->
```

父子销毁过程

```
vue2
父: beforeDestroy ->                              destroyed
子:                 beforeDestroy -> destroyed ->
```

```
vue3
父: beforeUnmount ->                            unmounted
子:                beforeUnmount -> unmounted ->
```




共分为4大类，8个钩子

- 创建前后  beforeCreate /  created
- 载入前后  beforeMount /  mounted
- 更新前后  beforeUpdate /  updated
- 销毁前后  beforeDestroy / destroyed,     beforeUnmount / unmounted

<br><br>
每个阶段的作用:

#### beforeCreate

是 new Vue() 之后触发的第一个钩子，在当前阶段 data、methods、computed 以及 watch 上的数据和方法都不能被访问。

#### created

在实例创建完成后发生，当前阶段已经完成了数据观测，也就是可以使用数据，data已经被创建, 更改数据，在这里更改数据不会触发 updated 函数。可以做一些初始数据的获取，在当前阶段无法与 Dom 进行交互，如果非要想，可以通过 vm.$nextTick 来访问 Dom。

#### beforeMount

发生在挂载之前，在这之前 template 模板已导入渲染函数编译。而当前阶段虚拟 Dom 已经创建完成，即将开始渲染, 但还是挂载之前为虚拟的 dom 节点，没有实际dom节点, 在此时也可以对数据进行更改，不会触发 updated。

#### mounted

在挂载完成后发生，在当前阶段，真实的 Dom 挂载完毕，数据完成双向绑定，可以访问到 Dom 节点，使用 $refs 属性对 Dom 进行操作。

#### beforeUpdate

发生在更新之前，也就是响应式数据发生更新，虚拟 dom 重新渲染之前被触发，你可以在当前阶段进行更改数据，不会造成重渲染。

#### updated

发生在更新完成之后，当前阶段组件 Dom 已完成更新。要注意的是避免在此期间更改数据，因为这可能会导致无限循环的更新。

#### beforeDestroy(beforeUnmount)

发生在实例销毁之前，在当前阶段实例完全可以被使用，我们可以在这时进行善后收尾工作，比如清除计时器。

#### destroyed(unmounted)

发生在实例销毁之后，这个时候只剩下了 dom 空壳。组件已被拆解，数据绑定被卸除，监听被移出，子实例也统统被销毁。



**如果需要发送异步请求，最好放在哪个钩子内？**

可以在钩子函数 created、beforeMount、mounted 中进行调用，因为在这三个钩子函数中，data 已经创建，可以将服务端端返回的数据进行赋值。

created中异步获取数据，最好是不适用watch监听的场景。否则created中数据返回了，watch监听还没创建。
能更快获取到服务端数据，减少页面 loading 时间,相当于 早一点发送了请求，等到页面加载完成后，请求也差不多要回来了。


而在mounted中获取数据，可以正常被watch监听。还是页面等待时间比较长。

ssr 不支持 beforeMount 、mounted 钩子函数，所以放在 created 中有助于一致性；