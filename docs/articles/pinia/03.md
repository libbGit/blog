store定义 ts

```js
import { defineStore } from "pinia";
import { stringify, parse } from "zipson";

interface State {
  counter: number,
  name: string,
  isAdmin: boolean,
}

export const useCounterStore = defineStore({
  id: "counter",
  state:():State  => ({
    // 所有这些属性都将自动推断其类型
    counter: 0,
    name: "Eduardo",
    isAdmin: true,
  }),

  getters: {
    doubleCount: (state:State) => state.counter * 2,
    doubleCountPlusOne() {
      // 通过this访问其他getter
      return this.doubleCount + 1;
    },
    getUserById: (state:State) => {
      //getUserById本身不接受参数，但是可以返回函数，用来接受参数
      return (userId) => state.users.find((user) => user.id === userId);
    },
    otherGetter(state:State) {
      //访问其他store的getter
      //顶部写 import { useOtherStore } from './other-store'
      // const otherStore = useOtherStore()
      // return state.localData + otherStore.data;
    },
  },
  actions: {
    increment() {
      //同步的
      this.counter++;
    },

    async registerUser(login:string, password:string) {
      //异步的
      try {
        // this.userData = await fetch('xxx')
      } catch (error) {}
    },

    async fetchUserPreferences(login:string, password:string) {
      //顶部写  import { useAuthStore } from './auth-store'
      // const auth = useAuthStore()
      try {
        // this.userData = await fetch('xxx')
      } catch (error) {}
    },
  },
  persist: {
    key: "counter-key",
    storage: window.sessionStorage,
    paths: ["counter", "name"],  //需要持久化的state
    serializer: {
      deserialize: parse,
      serialize: stringify,
    },
    beforeRestore: (context) => {
      console.log("Before hydration...");
    },
    afterRestore: (context) => {
      console.log("After hydration...");
    },
  },
});
```

