vue和react对比

|                        | vue语法                                                      | react语法                                                    |
| ---------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **文本插值**           | `{`{` name `}`}`                                                   | `{name}     `                                                  |
| **原始 HTML**          | `v-html="rawHtml"   `                                          | `let html = (123131)`<br />`{html}          `                    |
| **Attribute 绑定**     | `:id="dynamicId" `                                             | `id={ dynamicId }`                                             |
| **指令:v-if， v-else** | `v-if="seen"    v-else `                                       | `{seen? : }{ seen && } `                                       |
| **指令:v-for**         | ![image-20230506102752584](/public/imgs/image-20230506102752584.png) | ![image-20230506102825180](/public/imgs/image-20230506102825180.png) |
| 表单                   | `v-model `                                                     | ![image-20230506102848351](/public/imgs/image-20230506102848351.png) |
| **Class绑定**          | `:class="{ active: isActive }"`                                | `className={active} `                                          |
| **style绑定**          | `:style="{ color: activeColor }" `                             | `style={color:red} `                                           |
| **事件绑定**           | `@click="doSomething" `                                        | ![image-20230506102908362](/public/imgs/image-20230506102908362.png) |
| **事件触发**           | `this.$emit('submit')`                                         | `this.props.onClick`(12)                                       |
| **生命周期**           | ![image-20230506102922333](/public/imgs/image-20230506102922333.png) | ![image-20230506102935601](/public/imgs/image-20230506102935601.png) |
| **当前组件状态**       | `data(){  return {}} `                                         | `this.state = {};`    <br /> `let {num, setNum}=useState()`       |
| **计算属性**           | computed                                                     | ![image-20230506102956392](/public/imgs/image-20230506102956392.png) |
| **侦听器**             | watch                                                        | useEffect                                                    |
| **模板引用**           | `ref="input"`                                               | React.createRef()                                            |
| **Props 声明**         | ![image-20230506103014819](/public/imgs/image-20230506103014819.png) | ![image-20230506103026253](/public/imgs/image-20230506103026253.png) |
| **插槽 Slots**         | ![image-20230506103039071](/public/imgs/image-20230506103039071.png) | ![image-20230506103046352](/public/imgs/image-20230506103046352.png) |
| **依赖注入**           | ![image-20230506103100213](/public/imgs/image-20230506103100213.png) | ![image-20230506103112040](/public/imgs/image-20230506103112040.png) |
| 异步组件               | defineAsyncComponent                                         | `const OtherComponent = React.lazy(() => import('./OtherComponent'));` |
| 过渡                   | Transition、TransitionGroup                                  | react-transition-group 库                                    |
| 组件缓存               | KeepAlive                                                    | React.memo                                                   |
| 透传                   | Teleport                                                     | createPortal                                                 |
| 渲染完更新             | nextTick                                                     | setState，                                                   |
| fragment               | ![image-20230506103127400](/public/imgs/image-20230506103127400.png) | ![image-20230506103134705](/public/imgs/image-20230506103134705.png) |
| 高阶组件               | ![image-20230506103143450](/public/imgs/image-20230506103143450.png) | ![image-20230506103152726](/public/imgs/image-20230506103152726.png) |
| 路由参数传递           | $route                                                       | props中获取,                                                 |
| CSS处理                | 支持使用scoped属性来限定组件样式的作用域                     | 支持使用CSS Modules、styled-components等方式来处理组件样式   |
|                        |                                                              |                                                              |
