let topNav = [
  { text: "首页", link: "/index.md" },
  {
    text: "生态",
    link: "/articles/ecology",
  },
  {
    text: "基础",
    items: [
      { text: "HTML", link: "/articles/html/01" },
      { text: "CSS", link: "/articles/css/01" },
      { text: "SCSS", link: "/articles/scss/01" },
      { text: "JavaScript", link: "/articles/js/01" },
      { text: "TypeScript", link: "/articles/ts/01" },
    ],
  },
  { text: "HTTP", link: "/articles/http/01" },

  {
    text: "Vue",
    items: [
      { text: "Vue2", link: "/articles/vue2/01" },
      { text: "Vue3", link: "/articles/vue3/01" },
    ],
  },
  { text: "React", link: "/articles/react/01" },
  { text: "小程序", link: "/articles/miniprogram/01" },
  {
    text: "状态管理",
    items: [
      { text: "Vuex", link: "/articles/vuex/01" },
      { text: "Pinia", link: "/articles/pinia/01" },
    ],
  },
  {
    text: "构建",
    items: [
      { text: "Babel", link: "/articles/babel/01" },
      { text: "Webpack", link: "/articles/webpack/01" },
      { text: "Rollup", link: "/articles/rollup/01" },
      { text: "Esbuild", link: "/articles/esbuild/01" },
    ],
  },

  {
    text: "服务端",
    items: [
      { text: "Nginx", link: "/articles/nginx/01" },
      { text: "Nodejs", link: "/articles/nodejs/01" },
    ],
  },
  {
    text: "其他",
    items: [
      { text: "解决方案", link: "/articles/problem-solution/01" },
      { text: "设计模式", link: "/articles/design-pattern/01" },
      { text: "数据结构和算法", link: "/articles/algorithm/01" },
      { text: "Vscode", link: "/articles/vscode/01" },
      { text: "Monorepo", link: "/articles/monorepo/01" },
      { text: "Linux", link: "/articles/linux/01" },
      { text: "Github", link: "/articles/github/01" },
      { text: "开发规范", link: "/articles/standard/01" },
    ],
  },
];

export default topNav;
