javascript的生态，主要从编译器，打包器，包管理工具，库开发，应用开发等方面。下面便从这几个方便进行对比，从底层到高层来看

### 一、编译器

编译器负责将输入代码转换为某种目标输出格式。

出于我们的目的，我们专注于支持将现代 JavaScript 和 TypeScript 转换为与浏览器和 Node.js 最新版本兼容的 ECMAscript 特定版本的编译器。

![image-20230901170538324](/public/imgs/js/image-20230901170538324.png)

![image-20230907105557546](/public/imgs/js/image-20230907105557546.png)

除了 babel 编译时，需要插件@babel/preset-typescript 之外，其他的都不需要，内置编译 ts 的逻辑。

注意，不管 babel，esbuild，swc 编译 ts，都不执行 ts 的检查，ts 的检查，是由 typescript 执行。

![image-20230901172304775](/public/imgs/js/image-20230901172304775.png)

### 二、打包器

Bundler 负责获取所有输入源文件，并将它们打包成易于使用的输出格式。

打包器最常见的两个用例是：打包库 和 打包应用

![image-20230901181121053](/public/imgs/js/image-20230901181121053.png)

### 三、包安装管理工具

热门项目使用情况

<img src="/public/imgs/js/https___file.webp" alt="https___file" style="zoom: 50%;" />

### 四、库开发

下面这些工具可以帮助库作者快速打包和发布现代 npm 包

![image-20230901183133754](/public/imgs/js/image-20230901183133754.png)

推荐用 vite，能够将库打包成 esm, cjs, iife, amd, umd 等

### 五、web 应用开发

这些更高级的工具和框架旨在帮助开发人员构建现代 web 应用程序，而无需担心所有细节。

![image-20230907103752560](/public/imgs/js/image-20230907103752560.png)

**参考**

- https://bundlers.tooling.report
- https://transitivebullsh.it/javascript-dev-tools-in-2022
- https://2021.stateofjs.com/en-US/libraries/
- https://blog.logrocket.com/javascript-package-managers-compared
- https://loveky.github.io/2019/02/11/yarn-pnp/
