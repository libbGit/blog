定义一个tree型接口

```tsx
interface Tree {
  id: string
  label: string
  children?: Tree[]
}
```



举例

```js
let a: Tree[] = [
  {
    id: "1",
    label: "1",
    children: [{ id: "1", label: "1", children: undefined }],
  },
];
```

