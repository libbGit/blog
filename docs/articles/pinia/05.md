自定义持久化插件

```js
//利用函数柯里化接受用户入参
const piniaPlugin = (options) => {
  // let { key, storage } = options;
  const key = options?.key ?? "__piniaKey";
  const storage = options?.storage ?? window.sessionStorage;

  //将数据存在本地
  const setStorage = (id, value) => {
    let data = storage.getItem(key);
    data = data ? JSON.parse(data) : null;
    if (!data) {
      data = { [id]: value };
    } else {
      data[id] = value;
    }
    storage.setItem(key, JSON.stringify(data));
  };

  //存缓存中读取
  const getStorage = (id) => {
    let data = storage.getItem(key);
    return data ? JSON.parse(data) : {};
  };

  //将函数返回给pinia  让pinia  调用 注入 context
  return (context) => {
    const { store } = context;

    const data = getStorage(store.$id);

    store.$subscribe(() => {
      setStorage(store.$id, toRaw(store.$state));
    });

    //将data中的数据全部打开返回，
    let storeKeys = Object.keys(data);
    let tempData = {};
    for (let k of storeKeys) {
      tempData = { ...tempData, ...data[k] };
    }
    //返回值覆盖pinia 原始值,扁平化
    return tempData;
  };
};
```





```js
pinia.use(
  piniaPlugin({
    key: "pinia",
    storage: window.localStorage,
  })
); //安装插件
```

